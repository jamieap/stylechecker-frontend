# Stylechecker Frontend
## CO600 Group Project (jb695, jtk6, cbag2, ms693)

This is the web frontend of the code style checker for the 1st year module CO520


To build the project from source:

1. Clone the Git repository

2. Update the configuration file `config.json` with the backend connection details


To run the frontend copy the project contents to a web server leaving the existing directory structure in tact